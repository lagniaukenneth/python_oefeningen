veggies = {"bell pepper": 0.65, "tomato": 0.45}

print({veggies['bell pepper']})

person = {
    "f_name": "James",
    "l_name": "Bond",
    "age": 50,
    "old_fashioned": True
}

print(f"My name is {person['l_name']}")
print(f"My name is {person['age']}")
print(f"My name is {person['f_name']}")

from scrap import *

my_person = Person(
    "Kenneth",
    23,
    "Male",
    193,
    88,
    "Software Tester",
    "Spaghetti"
)

print(my_person.name)
print(my_person.age)
print(my_person.height)
print(my_person.mass)
print(my_person.favourite_food)

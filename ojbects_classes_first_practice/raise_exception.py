def make_pizza(toppings):
    msg = "Making a pizza with: "
    for topping in toppings:
        if topping == "pineapple":
            raise Exception("No pineapple on pizza allowed")
        else:
            msg += topping

    return msg


my_toppings = ["Pineapplen, ", "Hawai, ", "Pineapple, ", "Strawberrys"]
print(make_pizza(my_toppings))

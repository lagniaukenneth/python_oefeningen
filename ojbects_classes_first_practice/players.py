class Character:
    def __init__(self, name, total_hp, hp, attack):
        self.name = name
        self.total_hp = total_hp
        self.hp = hp
        self.attack = attack

    def introduce(self):
        return f"Hello i am {self.name}"


class Hero(Character):
    def __init__(self, name, total_hp, hp, attack):
        super().__init__(name, total_hp, hp, attack)

    def introduce(self):
        return super().introduce()

    def __str__(self):
        return f"Helloooo {self.name}"


class Monster(Character):
    def __init__(self, name, total_hp, hp, attack):
        super().__init__(name, total_hp, hp, attack)

    def introduce(self):
        return super().introduce()


class Dragon(Monster):
    def __init__(self, name, total_hp, hp, attack, colour, wingspan):
        super().__init__(name, total_hp, hp, attack)
        self.colour = colour
        self.wingspan = wingspan

    def fly(self):
        return f"I'm flying and it's cool!"

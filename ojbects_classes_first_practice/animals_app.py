import animals

pets = [
    animals.Dog("Rex", 11, "Shephard"),
    animals.Dog("Ringo", 11, "Shephard"),
    animals.Cat("Charlie", 11, "meow"),
    animals.Cat("Pablo", 11, "meow"),
    animals.Mouse("Tom", 11, "Shephard", "Woef!"),
    animals.Mouse("Jerry", 11, "Shephard", "Woef!"),
]


for pet in pets:
    print(pet.name)

for pet in pets:
    print(pet.make_sound())

for pet in pets:
    print(pet.age)









import car_oefening

cars = [
    car_oefening.Car("Blue", "Ford", "Focus"),
    car_oefening.Car("Red", "Ford", "Focus"),
    car_oefening.Car("Black", "Ford", "Focus")
]

for car in cars:
    print(f"I have a {car.colour} {car.model} {car.make}")

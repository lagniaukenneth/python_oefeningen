# import bank_account
#
# # my_bankaccount = bank_account.BankAccount(
# #     "BE1992",
# #     "1992",
# #     "Lagniau Kenneth",
# #     0
# # )
#
# bank_accounts = [
#     bank_account.BankAccount(
#         "BE1992",
#         "1992",
#         "Kenneth Lagniau",
#         0
#     ),
#     bank_account.BankAccount(
#         "BE1993",
#         "1993",
#         "Danny",
#         0
#     ),
#     bank_account.BankAccount(
#         "BE1994",
#         "1994",
#         "Jonas",
#         0
#     )
# ]
#
# # print(my_bankaccount.balance)
# #
# #
# # my_bankaccount.deposit(42)
# # print(my_bankaccount.balance)
# #
# # my_bankaccount.withdraw(33)
# # print(my_bankaccount.balance)
#
# print("********************************")
# print("********************************")
# print("**       ATM Application      **")
# print("********************************")
# print("********************************")
# print("********************************")
    #
#
# print("===== LOGIN SCREEN =====")
#
# while True:
#     input_id = input("Please enter your Account ID: ")
#     correct_id = False
#     my_bank_account = None
#     for account in bank_accounts:
#         correct_id = input_id == account.account_id
#         if correct_id:
#             my_bank_account = account
#             break
#     if correct_id:
#         attempts = 0
#         while True:
#             input_pin = input("Please enter your PIN code: ")
#             succesfully_logged_in = (input_pin == my_bank_account.pin_code)
#             if succesfully_logged_in:
#                 print(f"Welcome {my_bank_account.holder_name}, You have access to your Bank Account. ")
#                 while True:
#                     print(f"** Your balance: €{my_bank_account.balance: .2f} **")
#                     print("********************************")
#                     print("*1) Withdraw Money")
#                     print("*2) Deposit Money")
#                     print("*3) Exit")
#                     print("********************************")
#                     select_option = int(input("Select an option please (1-3): "))
#
#                     if select_option == 1:
#                         amount = float(input("How much do you want to withdraw?"))
#                         my_bank_account.withdraw(amount)
#                     elif select_option == 2:
#                         amount = float(input("How much do you want to deposit?"))
#                         my_bank_account.deposit(amount)
#                     elif select_option == 3:
#                         print("Logging out, Have a nice day! ")
#                         quit()
#                         break
#                 break
#             else:
#                 attempts += 1
#                 max_count = 3
#                 if attempts >= max_count - 1:
#                     print("Wrong ID / PIN. Try again... ")
#                     attempts += 1
#                 else:
#                     print("Enter your correct ID / PIN")
#                     break
#         else:
#             print("Wrong Account ID")




import bank_account

bank_accounts = [
    bank_account.BankAccount('BE32', '1234', "Lagniau Kenneth", 500),
    bank_account.BankAccount('BE33', '5678', "Danny", 5000),
    bank_account.BankAccount('BE34', '1111', "Jonas", 50)
]
print("****************************")
print("****************************")
print("**     ATM Application    **")
print("****************************")
print("****************************")
attempt_pin = 0
while True:
    bank = None
    correct_id = None
    input_id = input("What is your account ID? ")
    for account in bank_accounts:
        correct_id = (input_id == account.account_id)
        if correct_id:
            bank = account
            break
    if correct_id:
        while attempt_pin < 3:
            if input("What is your PIN? ") == bank.pin_code:
                while True:
                    print("****************************")
                    print(f"**  Welcome {bank.holder_name}.  **")
                    print("****************************")
                    print(f"** Your balance: €{bank.balance:.2f}  **")
                    print("****************************")
                    print("*1) Withdraw money         *")
                    print("*2) Deposit money          *")
                    print("*3) Exit                   *")
                    print("****************************")
                    choices = input("Select an option (1-3): ")
                    print("****************************")
                    if choices == '1':
                        bank.withdraw(int(input("How much money do you want to withdraw (EUR): ")))
                    elif choices == '2':
                        bank.deposit(int(input("How much money do you want to deposit (EUR): ")))
                    else:
                        print("*     Shutting down...     *")
                        print("****************************")
                        quit()
            else:
                print("Invalid PIN code, please try again...")
                attempt_pin += 1
        else:
            print("Too many failed attempts, card blocked.")
            quit()
    else:
        print("Invalid account ID, please try again...")









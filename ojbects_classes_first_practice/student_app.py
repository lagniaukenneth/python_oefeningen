import student

# my_students = student.Students[
#     student.Student("James Barnes", 12, "Leopoldstraat 20", [23, 65, 78]),
#     student.Student("James Bebe", 13, "Leopoldstraat 220", [23, 65, 78]),
#     student.Student("James Baba", 14, "Leopoldstraat 2220", [23, 65, 78]),
#     student.Student("James Bobo", 15, "Leopoldstraat 2220", [23, 65, 78])
# ]


my_student = student.Student(
    "James Barnes",
    11,
    "m",
    [22, 35, 98, 77]
)

print(f"Name: {my_student.name} & {my_student.age} Years old")
print(f"The average of grade =  {my_student.calc_grades_avg()}")


import players
import random
from players import *

hero = Hero("Supahman", 100, 100, 63)

monsters = [
    Monster("Danny1", 100, 100, 22),
    Monster("Danny2", 100, 100, 34),
    Monster("Danny3", 100, 100, 26)
]

rand_monster = monsters[random.randint(0, len(monsters) - 1)]

print(f"{hero.name} is facing off against a {rand_monster.name}")
print(hero.introduce)
print(rand_monster.introduce())
print(hero)



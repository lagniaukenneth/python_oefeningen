class Person:
    def __init__(self, name, age, gender, height, mass, occupation, favourite_food):
        self.favourite_food = favourite_food
        self.occupation = occupation
        self.mass = mass
        self.height = height
        self.gender = gender
        self.age = age
        self.name = name

    def walk(self):
        return "James barnes is going on a relaxing walk"

    def eat(self, food):
        return f"{self.name} is eating a {food}... "

    def sleep(self):
        return f"James Barnes is sleeping..ZzzzZzzz... "

    def work(self):
        return f"{self.name} is going to work as a {self.occupation}"

    def introduce(self):
        return f"Hello, my name is {self.name}. I'm a {self.age}-year-old man and i'm . " \
               f"My favourite food is " \
               f"{self.favourite_food} and i work as {self.occupation}"










# import bank_account
#
# def test_deposit_neg_num_rejected():
#     # Test case:
#     # Deposit -€1,00 to a balance
#     # Expected result: deposit rejected, balance unchanged (0)
#
#     # Arrange (Klaar zetten zeggen wat er moet gebeuren)
#     dummy_account = bank_account.BankAccount(
#         "test",
#         "test",
#         "test",
#         0
#     )
#     expected_balance = 0
#
#     # Act (Do my actual deposit (the thing i'm testing)
#     dummy_account.deposit(-1)
#     actual_balance = dummy_account.balance
#     # Assert (check whether the actual result is the same as my expected result)
#     if actual_balance == expected_balance:
#         return "Succes"
#     else:
#         return f"Test failed. expected: {expected_balance} your result: {actual_balance} "
#
#
#
# def test_deposit_zero_rejected():
#     # Test case:
#     # Deposit €0.00 to a balance 0
#     # Expected result: deposit rejected, balance unchanged(0)
#
#     dummy_account = bank_account.BankAccount(
#         "test",
#         "test",
#         "test",
#         0
#     )
#     expected_balance = 0
#     dummy_account.deposit(0)
#     actual_balance = dummy_account.balance
#
#     if actual_balance == expected_balance:
#         return "Succes"
#     else:
#         return f"Test failed. expected: {expected_balance} your result: {actual_balance} "
#
#
# def test_deposit_pos_num_accepted():
#     # Test case:
#     # Deposit €10,00 to a balance 0
#     # Expected result: deposit accepted balance changed(10)
#
#     dummy_account = bank_account.BankAccount(
#         "test",
#         "test",
#         "test",
#         0
#     )
#     expected_balance = 10
#     dummy_account.deposit(10)
#     actual_balance = dummy_account.balance
#
#     if actual_balance == expected_balance:
#         return "Succes"
#     else:
#         return "Fail!"
#
#
# def test_withdraw_neg_num_rejected():
#     dummy_account = bank_account.BankAccount(
#         "test",
#         "test",
#         "test",
#         0
#     )
#     expected_balance = 10
#     dummy_account.deposit(10)
#     actual_balance = dummy_account.balance
#
#     if actual_balance == expected_balance:
#         return "Succes"
#     else:
#         return "Fail!"
#
#
# def test_withdraw_more_then_balance():
# def test_withdraw_zero_rejected():
# def test_withdraw_pos_num_accepted():
#
# print(f"Running test #1: {test_deposit_neg_num_rejected()}")
# print(f"Running test #2: {test_deposit_zero_rejected()}")
# print(f"Running test #3: {test_deposit_pos_num_accepted()}")

from bank_account import *
from unittest import TestCase


class TestBankAccount(TestCase):
    def test_deposit_negative_num_rejected(self):
        # Test case:
        # Deposit -€1.00 to a balance of 0
        # Expected result: (Deposit rejected) - (Balance unchanged: 0) [Will not work since amount is higher then balance. ]
        my_dummy = BankAccount("test", "test", "test", 0)
        expected_balance = 0
        amount_deposit = -1

        my_dummy.deposit(amount_deposit)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)



    def test_deposit_zero_rejected(self):
        # Test case:
        # Deposit 0 to a balance of 0
        # Expected result: (Deposit rejected) - (Balance unchanged: 0)
        my_dummy = BankAccount("test", "test", "test", 0)
        expected_balance = 0
        amount_deposit = 0

        my_dummy.deposit(amount_deposit)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)


    def test_deposit_pos_num_accepted(self):
        # Test case:
        # Deposit 10 to a balance of 0
        # Expected result: (Deposit accepted) - (Balance changed: 10)
        my_dummy = BankAccount("test", "test", "test", 0)
        expected_balance = 10
        amount_deposit = 10

        my_dummy.deposit(amount_deposit)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)

    def test_withdraw_negative_num_rejected(self):
        # Test case:
        # Withdraw -1 to a balance of 100
        # Expected result: (Deposit accepted) - (Balance changed: 100)
        my_dummy = BankAccount("test", "test", "test", 100)
        expected_balance = 100
        amount_withdraw = -1

        my_dummy.withdraw(amount_withdraw)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)




    def test_withdraw_zero_rejected(self):
        # Test case:
        # Withdraw 0 to a balance of 100
        # Expected result: (Deposit accepted) - (Balance changed: 100)
        my_dummy = BankAccount("test", "test", "test", 100)
        expected_balance = 100
        amount_withdraw = 0

        my_dummy.withdraw(amount_withdraw)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)




    def test_withdraw_pos_num_accepted(self):
        # Test case:
        # Withdraw 10 to a balance of 100
        # Expected result: (Deposit accepted) - (Balance changed: 100)
        my_dummy = BankAccount("test", "test", "test", 100)
        expected_balance = 90
        amount_withdraw = 10

        my_dummy.withdraw(amount_withdraw)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)




    def test_withdraw_higher_num_rejected(self):
        # Test case:
        # Withdraw 100.01 to a balance of 100
        # Expected result: (Deposit accepted) - (Balance changed: 100)
        my_dummy = BankAccount("test", "test", "test", 100)
        expected_balance = 100
        amount_withdraw = 100.01

        my_dummy.withdraw(amount_withdraw)
        actual_balance = my_dummy.balance
        self.assertEqual(expected_balance, actual_balance)







# Arrange, Act, Assert
# ====================
# Arrange
# Act
# Do my actual deposit (The thing I'm testing)
# Assert
# Check if my actual result is the same as my expected result
# ===========================================================


# def test_deposit_negative_num_rejected():
#     # Test case:
#     # Deposit -€1.00 to a balance of 0
#     # Expected result: (Deposit rejected) - (Balance unchanged: 0) [Will not work since amount is higher then balance. ]
#     expected_result = 0
#     amount_deposit = -1
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 0)
#     dummy_account.deposit(amount_deposit)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# def test_deposit_zero_rejected():
#     # Test case:
#     # Deposit 0 to a balance of 0
#     # Expected result: (Deposit rejected) - (Balance unchanged: 0)
#     expected_result = 0
#     amount_deposit = 0
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 0)
#     dummy_account.deposit(amount_deposit)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# def test_deposit_pos_num_accepted():
#     # Test case:
#     # Deposit 10 to a balance of 0
#     # Expected result: (Deposit accepted) - (Balance changed: 10)
#     expected_result = 10
#     amount_deposit = 10
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 0)
#     dummy_account.deposit(amount_deposit)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# def test_withdraw_negative_num_rejected():
#     # Test case:
#     # Withdraw -1 to a balance of 100
#     # Expected result: (Deposit accepted) - (Balance changed: 100)
#     expected_result = 100
#     amount_withdraw = -1
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 100)
#     dummy_account.withdraw(amount_withdraw)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# def test_withdraw_zero_rejected():
#     # Test case:
#     # Withdraw 0 to a balance of 100
#     # Expected result: (Deposit accepted) - (Balance changed: 100)
#     expected_result = 100
#     amount_withdraw = 0
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 100)
#     dummy_account.withdraw(amount_withdraw)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# def test_withdraw_pos_num_accepted():
#     # Test case:
#     # Withdraw 10 to a balance of 100
#     # Expected result: (Deposit accepted) - (Balance changed: 100)
#     expected_result = 90
#     amount_withdraw = 10
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 100)
#     dummy_account.withdraw(amount_withdraw)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# def test_withdraw_higher_num_rejected():
#     # Test case:
#     # Withdraw 100.01 to a balance of 100
#     # Expected result: (Deposit accepted) - (Balance changed: 100)
#     expected_result = 100
#     amount_withdraw = 100.01
#     dummy_account = bank_account.BankAccount("BE1234", "0000", "Dummy 1", 100)
#     dummy_account.withdraw(amount_withdraw)
#     actual_result = dummy_account.balance
#     if expected_result == actual_result:
#         return f"Passed"
#     else:
#         return f"Failed: expected result {expected_result}, got {actual_result}"
#
#
# print(f"Running test case #1 (Deposit negative num to balance): {test_deposit_negative_num_rejected()}")
# print(f"Running test case #2: (Deposit zero to balance): {test_deposit_zero_rejected()}")
# print(f"Running test case #3: (Deposit 10 to balance): {test_deposit_pos_num_accepted()}")
# print(f"Running test case #4: (Withdraw negative num from balance): {test_withdraw_negative_num_rejected()}")
# print(f"Running test case #5: (Withdraw zero from balance): {test_withdraw_zero_rejected()}")
# print(f"Running test case #6: (Withdraw 10 from balance): {test_withdraw_pos_num_accepted()}")
# print(f"Running test case #7: (Withdraw higher num then balance): {test_withdraw_higher_num_rejected()}")

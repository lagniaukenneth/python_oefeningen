class Product:
    def __init__(self, name, amount, price):
        self.name = name
        self.amount = amount
        if price > 0:
            self.price = price


class ShoppingList:
    def __init__(self, products):
        self.products = products

    def add_item(self, product):
        self.products.append(product)

    def total_cost(self):
        total_cost = 0
        for product in self.products:
            total_cost += product.price
        return total_cost


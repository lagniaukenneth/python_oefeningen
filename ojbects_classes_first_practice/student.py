class Student:
    def __init__(self, name, age, address, grades):
        self.name = name
        self.age = age
        self.address = address
        self.grades = grades

    def calc_grades_avg(self):
        total = sum(self.grades)
        amount = len(self.grades)
        average = total / amount
        return average


# class Student2:
#     def __init__(self, name, age, gender, address, class_group, grades):
#         self.name = name
#         self.age = age
#         self.gender = gender
#         self.address = address
#         self.class_group = class_group
#         self.grades = grades

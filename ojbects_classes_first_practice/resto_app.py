import resto

#restaurant.Restaurant << eerste restaurant is verwezen naar de import class
dannys_resto = resto.Restaurant(
    "PizzaPaste",
    "Italiaans",
    [
        "Pasta pesto",
        "lookbrood",
        "pasta"
    ]
)
# item volledig verwijderen en nieuwe toevoegen tussen ""
dannys_resto.menu_items[1] = "Nieuwe pasta"
dannys_resto.menu_items.append("Pizza Hawai")


print(dannys_resto.name)
print(dannys_resto.menu_items[1])

for item in dannys_resto.menu_items[2]:
    print(item)

for item in dannys_resto.menu_items:
    print(item)

for name in dannys_resto.name:
    print(name)


# dannys_resto.menu_items.remove("Pizza Hawai")
# hier onder is het dezelfde als boven.!!!! de del manier is performanter
del dannys_resto.menu_items[3]

# manier om iets toe te voegen aan de lijst via functie beschreven in class als method.
dannys_resto.add_menu_item("Pizza Kebab")
print(dannys_resto.menu_items)

print("=======================================")

kenneth_resto = resto.RestaurantsNinove(
    "Malt",
    [
        resto.MenuItem("Lookbrood", 3.50),
        resto.MenuItem("Spaghetti Bolognaise", 13.50),
        resto.MenuItem("Pasta Scampi", 23.50),
        resto.MenuItem("Spaghetti met hamburgers", 24.99),
        resto.MenuItem("Bruschetta", 4.50),
    ] # dit kan ook [] een lege lijst zijn en vervolgens toevoegen met de nodige commands.
)

for item in kenneth_resto.menu_items:
    print(f"{item.name} {item.price:.2f}")

print(kenneth_resto.name)

del kenneth_resto.menu_items[0]

kenneth_resto.add_menu_item(resto.MenuItem("Pizza Hawaii", 14.50))

kenneth_resto.menu_items[1] = resto.MenuItem("Pasta pesto", 18.00)

print(f"The price of {kenneth_resto.menu_items[2].name} is... {kenneth_resto.menu_items[2].price:.2f} Euro")

wants_to_continue = True
while wants_to_continue:
    input_name = input("Enter item's name: ")
    input_price = float(input("Enter price of your item"))

    input_menu_item = resto.MenuItem(input_name, input_price)
    kenneth_resto.add_menu_item(input_menu_item)

    wants_to_continue = input("Do you want to add another item?(y/n)? ") == "y"

for item in kenneth_resto.menu_items:
    print(f"{item.name} is ready to serve for the price: {item.price:.2f}")

# for i in kenneth_resto.menu_items:
#     print(i)
#
# # kenneth_resto.add_menu_item("Pizza Hawaii", 23)
# # kenneth_resto.add_menu_item("Kebaba", 44)
#
# print(kenneth_resto.menu_items)
#
# while True:
#     input_menu_item = input("Enter your menu item: ")
#     if input_menu_item == "":
#         print("You have to enter something dummy!" )
#         break
#     else:
#         kenneth_resto.add_menu_item(input_menu_item)
#
# print(kenneth_resto.menu_items)














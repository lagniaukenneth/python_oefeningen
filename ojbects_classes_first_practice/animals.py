class Pet:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def make_sound(self):
        return f"{self.name} woefiwoef"


class Dog(Pet):
    def __init__(self, name, age, breed):
        super().__init__(name, age)
        self.breed = breed

    def make_sound(self):
        return "Woef"


class Cat(Pet):
    def __init__(self, meow, name, age):
        super().__init__(name, age)
        self.meow = meow

    def make_sound(self):
        return "Miauuww"


class Mouse(Pet):
    def __init__(self, name, age, favourite_cheese, squeak):
        super().__init__(name, age)
        self.squeak = squeak
        self.favourite_cheese = favourite_cheese

    def make_sound(self):
        return "Squaaaak!"







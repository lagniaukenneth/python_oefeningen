class Chair:
    def __init__(self, name, material, amount_legs, price, kind, colour):
        self.name = name
        self.material = material
        self.amount_legs = amount_legs
        self.price = price
        self.kind = kind
        self.colour = colour

    def adjust(self, height):
        return print(f"You have adjusted your chair at his {height}.")

    def rotate(self, degrees):
        return print(f"You have adjusted your chair at his {degrees}.")


class Manufacturer:
    def __init__(self, name, location):
        self.location = location
        self.name = name

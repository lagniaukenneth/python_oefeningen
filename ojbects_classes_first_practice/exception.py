def divide(a, b):
    if a == 0 or b == 0:
        raise Exception("Cannot divide zero or by zero")
    else:
        return a/b


print(divide(42, 0))


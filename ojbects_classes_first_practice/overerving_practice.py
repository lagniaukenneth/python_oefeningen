import players

my_hero = players.Hero(
    "Kenneth",
    300,
    300,
    50
)

monsters = [
    players.Monster("Cell", 1500, 1250, 50),
    players.Monster("Sjaak", 1500, 1250, 50),
    players.Monster("Cell2", 1500, 1250, 50),
    players.Dragon("Krimi", 1500, 1250, 50, "red", 30),
]

print(my_hero.name)

for monster in monsters:
    print(monster.name)


import datetime


def greet(name):
    now = datetime.datetime.now()
    if (now.hour >= 6) and (now.hour < 12):
        return f"Good morning! {name}"
    elif (now.hour >= 12) and (now.hour < 16):
        return f"Good afternoon! {name}"
    elif (now.hour >= 16) and (now.hour < 21):
        return f"Good evening! {name}"
    elif (now.hour >= 21) and (now.hour < 6):
        return f"Good night! {name}"
    else:
        return f"Invalid hour {name}"


print(greet(f"Kenneth, het is {now.hour}u !"))




# print(f"::{now.month}/{now.day}/{now.year}:: Hour: {now.hour} Minutes: {now.minute} Seconds: {now.second}")
#
# if (now.hour >= 6) and (now.hour <= 12):
#     print("Good morning!")
# elif (now.hour >= 12) and (now.hour <= 16):
#     print("Good afternoon!")
# if (now.hour >= 16) and (now.hour <= 21):
#     print("Good evening!")
# if (now.hour >= 21) and (now.hour <= 6):
#     print("Good night!")





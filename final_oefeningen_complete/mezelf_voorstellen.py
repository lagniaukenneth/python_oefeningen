def introduce(f_name, l_name, hobbies):
    print(f"Hello my name is {f_name} {l_name} and my hobbies are ", end="")
    index = 0
    for hobby in hobbies:
        # if it's the last hobby
        if index == len(hobbies) - 1:
            print(f"and {hobby}.")
        else:
            print(f"{hobby}, ", end="")
        index += 1


def check_valid_drinking_age(age):
    if age < 18:
        print("You are not allowed to drink")
    else:
        print("You're cool")


f_name = input("Enter your first name: ")
l_name = input("Enter your last name: ")
age = int(input("Enter your age: "))

hobbies = []

while True:
    hobby = input("Enter your hobby: ")
    if hobby == '':
        break
    else:
        hobbies.append(hobby)

user = {
    "f_name": f_name,
    "l_name": l_name,
    "age": age,
    "hobbies": hobbies
}

check_valid_drinking_age(age)
introduce(user['f_name'], user['l_name'], user['hobbies'])

quiz_questions = [
    {
        "question": "What is the capital of Belgium ?",
        "choices": ["Brugge","Leuven","Antwerpen","Brussel"],
        "answer": "Brussel",
        "points": 3
    },
    {
        "question": "What is the largest fish in sea ?",
        "choices": ["Shark", "Garnaaltjes", "Goudviske", "Paling"],
        "answer": "Shark",
        "points": 3
    },
    {
        "question": "What is the steepest mountain in Europe ?",
        "choices": ["Col du rat", "De Muur", "Ventoux", "Paterberg"],
        "answer": "De Muur",
        "points": 3
    },
    {
        "question": "What is the biggest concertzaaltje in Belgium ?",
        "choices": ["Capitool", "Sportpaleis", "LottoArena", "H20"],
        "answer": "Sportpaleis",
        "points":  3,
    },

]

total_points = 0
total_possible_points = 0
for quiz in quiz_questions:
    print(f"The next question is: {quiz['question']}")
    for choice in quiz['choices']:
        print(f"* {choice} *")
    input_answer = input("Enter your answer: ")
    total_possible_points += quiz['points']
    if input_answer == quiz["answer"]:
        total_points = total_points + quiz["points"]
        print("Correct! on to the next... ")
    else:
        print("Wrong you lose!")
        print(f"You've scored {total_points}")

print(f"Your total score is: {total_points} out of {total_possible_points}")


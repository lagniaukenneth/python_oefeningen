import random


# Shuffled is a boolean that determines if the playing cards should be shuffled or not
def generate_deck(shuffled=False):
    playing_cards = []
    suits = ["clubs", "spades", "hearts", "diamonds"]
    for suit in suits:
        for n in range(1, 14):
            if n == 1:
                playing_cards.append(f"Ace of {suit}")
            elif n == 11:
                playing_cards.append(f"Jack of {suit}")
            elif n == 12:
                playing_cards.append(f"Queen of {suit}")
            elif n == 13:
                playing_cards.append(f"King of {suit}")
            else:
                playing_cards.append(f"{n} of {suit}")

    if shuffled:
        random.shuffle(playing_cards)
    return playing_cards


# (True) om naar de functie te verwijzen met boolean zoals beschreven bovenaan.


# suits = ["clubs", "spades", "hearts", "diamonds"]
#
# for suit in suits:
#     for n in range(1, 14):
#         if n == 1:
#             playing_cards.append(f"Ace of {suit}")
#         elif n == 11:
#             playing_cards.append(f"Jack of {suit}")
#         elif n == 12:
#             playing_cards.append(f"Queen of {suit}")
#         elif n == 13:
#             playing_cards.append(f"King of {suit}")
#         else:
#             playing_cards.append(f"{n} of {suit}")

playing_cards = generate_deck(True)
for card in playing_cards:
    print(card)



for n in range(1, 101):
    text1 = "Fizz"
    text2 = "Buzz"
    combo = "FizzBuzz"
    # Als meervoud van 3 --> FizzBuzz
    if n % 3 == 0 and n % 5 == 0:
        print(combo)
    # Als meervoud van 5 --> Buzz
    elif n % 5 == 0:
        print(text2)
    # Als meervoud van zowel 3  --> Fizz
    elif n % 3 == 0:
        print(text1)
    else:
        print(n)
    # Anders




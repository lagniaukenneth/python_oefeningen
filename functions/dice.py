import random


def roll_d(n):
    return random.randint(1, n)


def roll_d6():
    rand_num = random.randint(1, 6)
    return rand_num


def roll_d20():
    rand_num = random.randint(1, 20)
    return rand_num


#def roll_d20(): -----> je kan je functies ook zo met elkaar oproepen en vervolgens onmiddelijk roll_d returnen.
    #return roll_d(20)






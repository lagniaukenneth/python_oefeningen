import dice

print("The great eye of Sauron")

stealth_check = dice.roll_d20()

print(f"You rolled a {stealth_check}")
if stealth_check >= 10:
    print("You are free to go")
else:
    print("On their way!")
fruits = ["apple", "pear", "kiwi", "grape", "banana", "orange", "pineapple"]

print("I reach into the bag and the first fruit I grab is a " + fruits[4])

print("My favourite fruit is " + fruits[4])

print("I have " + str(len(fruits)) + " fruits in my bag")

print("last fruit in my bag is a " + fruits[len(fruits) - 1])

#toevoegen fruit (op positie of einde)

fruits.append("Aardbeitjes")
print(fruits)

fruits.insert(3, "BeschimmeldeAppeltjes")
print(fruits)

#verwijderen op basis van index
del fruits[2]
print(fruits)

fruits.insert(2, "Appelino's")

print(fruits)

#ALLES IN COMMENT ZETTEN : selecteren en dan control + forward slash


fruits = ["apple", "pear", "kiwi", "grape", "banana", "orange", "pineapple"]

counter = 0

while True:
    print(fruits[counter])
    counter += 1
    if counter > len(fruits) - 1:
        break


# ook mogelijk

counter = 0

while counter <= len(fruits) - 1:
    print(fruits[counter])
    counter += 1










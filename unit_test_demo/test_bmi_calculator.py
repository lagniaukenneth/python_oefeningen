from bmi_calculator import *
from unittest import TestCase


class TestBmiCalculator(TestCase):
    def test_calc_bmi(self):

        mass = 70
        height = 1.70
        expected_result = 24.2

        actual_result = calc_bmi(mass, height)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_underweight_upper(self):
        # Test case:
        # calculate BMI with values 53kg and 1.63m
        # should return a BMI of 19.9 (according to the formula)
        bmi = 19.9
        expected_result = "underweight"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_ok_lower(self):
        # Test case:
        # calculate BMI with values 61kg and 1.74m
        # should return a BMI of 20.1 (according to the formula)
        bmi = 20
        expected_result = "ok"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_ok_upper(self):
        # Test case:
        # calculate BMI with values 63kg and 1.59m
        # should return a BMI of 24.9 (according to the formula)
        bmi = 24.9
        expected_result = "ok"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_overweight_lower(self):
        # Test case:
        # calculate BMI with values 61kg and 1.56m
        # should return a BMI of 25.1 (according to the formula)
        bmi = 25
        expected_result = "overweight"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_overweight_upper(self):
        # Test case:
        # calculate BMI with values 99kg and 1.82m
        # should return a BMI of 29.9 (according to the formula)
        bmi = 29.9
        expected_result = "overweight"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_obese_lower(self):
        # Test case:
        # calculate BMI with values 81kg and 1.64m
        # should return a BMI of 30.1 (according to the formula)
        bmi = 30.0
        expected_result = "obese"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_obese_upper(self):
        # Test case:
        # calculate BMI with values 132kg and 1.82m
        # should return a BMI of 39.9 (according to the formula)
        bmi = 39.9
        expected_result = "obese"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

    def test_calc_bmi_morbid_lower(self):
        # Test case:
        # calculate BMI with values 130kg and 1.80m
        # should return a BMI of 40.1 (according to the formula)
        bmi = 40
        expected_result = "mordibly obese"
        actual_result = determine_bmi_category(bmi)
        self.assertEqual(expected_result, actual_result)

# import calculator
#
# def test_square():
#     num = 6
#     expected_result = 36
#     actual_result = calculator.square(test_num)
#
#     #Als ze overeenkomen > print succes!
#     #Als ze niet overeenkomen > print fail!
#
#     if actual_result == expected_result:
#         return "Succes!"
#     else:
#         return f"Failed: expected {expected_result}, got {actual_result}"
#
#
# print(f"test_square {test_square()}")
#

import bmi_calculator


def test_calc_bmi():
    # Test case:
    # calculate BMI with values 59kg and 1.70m
    # should return a BMI of 20.4 (according to the formula)
    mass = 59
    height = 1.70
    expected_result = 24.2
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test"
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_underweight_upper():
    # Test case:
    # calculate BMI with values 53kg and 1.63m
    # should return a BMI of 19.9 (according to the formula)
    mass = 53
    height = 1.63
    expected_result = 19.9
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_ok_lower():
    # Test case:
    # calculate BMI with values 61kg and 1.74m
    # should return a BMI of 20.1 (according to the formula)
    mass = 61
    height = 1.74
    expected_result = 20.0
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_ok_upper():
    # Test case:
    # calculate BMI with values 63kg and 1.59m
    # should return a BMI of 24.9 (according to the formula)
    mass = 63
    height = 1.59
    expected_result = 24.9
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_overweight_lower():
    # Test case:
    # calculate BMI with values 61kg and 1.56m
    # should return a BMI of 25.1 (according to the formula)
    mass = 61
    height = 1.56
    expected_result = 25.1
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_overweight_upper():
    # Test case:
    # calculate BMI with values 99kg and 1.82m
    # should return a BMI of 29.9 (according to the formula)
    mass = 99
    height = 1.82
    expected_result = 29.9
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_obese_lower():
    # Test case:
    # calculate BMI with values 81kg and 1.64m
    # should return a BMI of 30.1 (according to the formula)
    mass = 81
    height = 1.64
    expected_result = 29.9
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_obese_upper():
    # Test case:
    # calculate BMI with values 132kg and 1.82m
    # should return a BMI of 39.9 (according to the formula)
    mass = 132
    height = 1.82
    expected_result = 30.1
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


def test_calc_bmi_morbid_lower():
    # Test case:
    # calculate BMI with values 130kg and 1.80m
    # should return a BMI of 40.1 (according to the formula)
    mass = 130
    height = 1.80
    expected_result = 40.0
    actual_result = bmi_calculator.calc_bmi(mass, height)
    if actual_result == expected_result:
        return "Passed the test."
    else:
        return f"Failed: expected {expected_result}, got {actual_result}"


print(f"Running test #1 (calc BMI): {test_calc_bmi()}")
print(f"Running test #2 (calc BMI underweight upper): {test_calc_bmi_underweight_upper()}")
print(f"Running test #3 (calc BMI ok lower): {test_calc_bmi_ok_lower()}")
print(f"Running test #4 (calc BMI ok upper): {test_calc_bmi_ok_upper()}")
print(f"Running test #5 (calc BMI overweight lower): {test_calc_bmi_overweight_lower()}")
print(f"Running test #6 (calc BMI overweight upper): {test_calc_bmi_overweight_upper()}")
print(f"Running test #7 (calc BMI obese lower): {test_calc_bmi_obese_lower()}")
print(f"Running test #8 (calc BMI obese upper): {test_calc_bmi_obese_upper()}")
print(f"Running test #9 (calc BMI morbid lower): {test_calc_bmi_morbid_lower()}")

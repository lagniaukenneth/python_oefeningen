def determine_bmi_category(bmi):
    if bmi < 20:
        return f"underweight"
    elif bmi < 25:
        return f"ok"
    elif bmi < 30:
        return f"overweight"
    elif bmi < 40:
        return f"obese"
    else:
        return f"mordibly obese"


def calc_bmi(mass, height):
    bmi = mass / (height * height)
    return bmi
